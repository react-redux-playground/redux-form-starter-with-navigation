import React from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';

import {
    Button, 
    TextField, 
    Typography,
    Grid
} from '@material-ui/core';


class ForgotPasswordForm extends React.Component {

    renderError({ touched, error }) {
        if(touched && error) {
            return(
                <span className='text-danger'>{error}</span>
            )
        } 
    }

    renderMatInput = ({input, label, name, isFocus, type, meta}) => {
        return (
           <div>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id={name}
                    label={label}
                    name={name}
                    autoFocus={isFocus}
                    type={type}
                    {...input}
                />
                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = (formValues) => {
        console.log(formValues);
    }
    render() {
        return(
            <React.Fragment>
                <Typography component="h1" variant="h5">
                    Forgot Password
                </Typography>
                <form className="forgotPasswordForm" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field 
                        component={this.renderMatInput}
                        name="email"
                        label="Email address"
                        type="email"
                        isFocus={true}
                    />
                    <div className="submitBtn">
                        <Button type="submit" fullWidth variant="contained" color="primary">
                            Submit
                        </Button>
                    </div>

                    <Grid container>
                        <Grid item xs>
                            <Link to="/" variant="body2">
                            Back to login page
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </React.Fragment>
        )
    }
}

export default reduxForm({
    form: 'forgotPasswordForm'
})(ForgotPasswordForm);