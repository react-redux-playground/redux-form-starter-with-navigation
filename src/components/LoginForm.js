import React from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';

import {
    Grid,
    TextField,
    Button,
    FormControlLabel,
    Avatar,
    Typography,
    Checkbox
} from '@material-ui/core';
import LockOutlined from '@material-ui/icons/LockOutlined';


class LoginForm extends React.Component {

    renderError({ touched, error }) {
        if(touched && error) {
            return(
                <span className='text-danger'>{error}</span>
            )
        } 
    }

    renderMatInput = ({input, label, name, isFocus, type, meta}) => {
        return (
           <div>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id={name}
                    label={label}
                    name={name}
                    autoFocus={isFocus}
                    type={type}
                    {...input}
                />
                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = (formValues) => {
        console.log(formValues);
    }

    render() {
        return (
            <React.Fragment>
                <Avatar className="avatar">
                    <LockOutlined />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>

                <form className="loginForm" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field 
                        component={this.renderMatInput}
                        name="email"
                        label="Email address"
                        type="email"
                        isFocus={true}
                    />
                    <Field 
                        component={this.renderMatInput}
                        name="password"
                        label="Password"
                        type="password"
                        isFocus={false}
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    /> 
                    <div className="signInBtn">
                        <Button type="submit" fullWidth variant="contained" color="primary">
                            Sign In
                        </Button>
                    </div>

                    <Grid container>
                        <Grid item xs>
                            <Link to="/forgotPassword" variant="body2">
                            Forgot password?
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link to="/signUp" variant="body2">
                            {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </React.Fragment>
        )
    }
}

const validate = values => {
    const errors = {};

    if (!values.email) {
      errors.email = 'Email is required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (!values.password) {
        errors.password = 'Password is required'
    } 

    return errors;
}

export default reduxForm({
    form: 'loginForm',
    validate
})(LoginForm);