import React from 'react';
import { BrowserRouter , Route } from 'react-router-dom';

import LoginForm from './LoginForm';
import ForgotPasswordForm from './ForgotPasswordForm';
import SignupForm from './SignupForm';

import {
    Grid,
    CssBaseline,
    Paper
} from '@material-ui/core';



class Login extends React.Component {
    render () {
        return(
           <React.Fragment>
                <Grid container component="main" className="root">
                    <CssBaseline />  
                    <Grid item xs={false} sm={4} md={7} className="loginImage" />
                    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                        <div className="loginContainer">
                            <BrowserRouter>
                                <Route exact path='/' component={LoginForm}></Route>
                                <Route path='/forgotPassword' component={ForgotPasswordForm}></Route>
                                <Route path='/signUp' component={SignupForm}></Route>
                            </BrowserRouter>
                        </div>
                    </Grid>
                </Grid>
           </React.Fragment>
        )
    }
}

export default Login;