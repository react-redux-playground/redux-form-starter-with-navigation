import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';

import {
    Grid,
    TextField,
    Typography,
    Button
} from '@material-ui/core';


class SignupForm extends React.Component {
    renderError({touched, error}) {
        if(touched && error) {
            return(
                <span className='text-danger'>{error}</span>
            )
        }
    }

    renderMatInput = ({input, label, name, isFocus, type, meta}) => {
        return (
           <div>
                <TextField
                    error={meta.error && meta.touched}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id={name}
                    label={label}
                    name={name}
                    autoFocus={isFocus}
                    type={type}
                    {...input}
                />
                {this.renderError(meta)}
            </div>
        )
    }

    renderButton() {
        return(
            <div className="submitBtn">
                <Button
                    disabled={!this.props.valid}
                    component={Link}
                    to="/"
                    type="submit" 
                    fullWidth 
                    variant="contained" 
                    color="primary">
                        Sign Up
                </Button>  
            </div>
        )
    }

    onSubmit(formValues) {
        console.log('aaa', formValues);
    }

    render() {
        return(
            <React.Fragment>
                <Typography component="h1" variant="h5">
                    Sign Up
                </Typography>
                <form className="forgotPasswordForm" 
                        onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <Field 
                                component={this.renderMatInput}
                                name="firstName"
                                label="First Name"
                                type="text"
                                isFocus={false}
                            />
                        </Grid>

                        <Grid item xs={6}>
                            <Field 
                                component={this.renderMatInput}
                                name="lastName"
                                label="Last Name"
                                type="text"
                                isFocus={false}
                            />
                        </Grid>
                    </Grid>

                    <Grid container >
                        <Grid item xs={12}>
                            <Field 
                                component={this.renderMatInput}
                                name="email"
                                label="Email"
                                type="email"
                                isFocus={false}
                            />
                        </Grid>
                    </Grid>
                    {this.renderButton()}

                </form>
            </React.Fragment>
        )
    }
}

const validate = (formValues) => {
    const errors = {};

    if(!formValues.firstName)
        errors.firstName = 'First name is required';

    if(!formValues.lastName)
        errors.lastName = 'Last name is required';

    if (!formValues.email) {
        errors.email = 'Email is required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.email)) {
        errors.email = 'Invalid email address'
    }

    return errors;
}

export default reduxForm ({
    form: 'signUpForm',
    validate
})(SignupForm);