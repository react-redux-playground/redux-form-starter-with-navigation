import { combineReducers } from 'redux';
import { reducer as FormReducers } from 'redux-form';

export default combineReducers( {
    form: FormReducers
});